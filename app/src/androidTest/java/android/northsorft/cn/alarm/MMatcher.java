package android.northsorft.cn.alarm;

import android.view.View;

import com.getbase.floatingactionbutton.FloatingActionButton;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.lang.reflect.Field;

/**
 * Created by Xushudi on 2016/12/22 上午11:41.
 */

public class MMatcher {
    public static Matcher<View> isDrawableNotifyON(final int stat){
        return new TypeSafeMatcher<View>() {

            @Override
            public void describeTo(Description description) {
                if (stat==1){
                    description.appendText("图标不为激活状态错误");
                }else {
                    description.appendText("图标不为非激活状态错误");
                }
            }

            @Override
            protected boolean matchesSafely(View item) {
                Field field;
                int icon = 0;
                try {
                    field = FloatingActionButton.class.getDeclaredField("mIcon");
                    field.setAccessible(true);
                    FloatingActionButton floatingActionButton = (FloatingActionButton) item;
                    icon = (int) field.get(floatingActionButton);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }
                if (stat==1){
                    return icon==R.drawable.ic_notifications_on_white_24dp;
                }else {
                    return icon==R.drawable.ic_notifications_off_grey600_24dp;
                }
            }
        };
    }

}
