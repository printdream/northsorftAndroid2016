package android.northsorft.cn.alarm;
import android.northsorft.cn.alarm.ui.activity.MainActivity;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.app.AlertDialog;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withResourceName;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Xushudi on 2016/12/23 下午8:20.
 */
@RunWith(AndroidJUnit4.class)
public class EditAlarmActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);
    @Test
    public void selectRepeatOnclick() throws Exception {
        // TODO: 2016/12/21 测试未通过
        // 编辑Item
        onView(withText("test1")).perform(click());
        // 点击周期按钮
        onView(withId(R.id.repeat_icon)).perform(click());
        // 检测是否显示Dialog
        onView(withText("选择重复")).check(matches(isDisplayed()));
        // 检测Dialog是否显示"只响一次"
        onView(withText("只响一次")).check(matches(isDisplayed()));
        // 点击Dialog"只响一次"
        onView(withText("只响一次")).perform(click());
        // 检查View中是否包含只响一次
        onView(withText("只响一次")).check(matches(isDisplayed()));
    }

    @Test
    public void selectRingOnclick() throws Exception {
        // TODO: 2016/12/21 测试未通过
        // 编辑Item
        onView(withText("test1")).perform(click());
        // 点击闹钟方式按钮
        onView(withId(R.id.holdSleep_icon)).perform(click());
        // 检测是否显示Dialog
        onView(withText("选择方式")).check(matches(isDisplayed()));
        // 检测Dialog是否显示"震动"
        onView(withText("震动")).check(matches(isDisplayed()));
        // 点击Dialog"震动"
        onView(withText("震动")).perform(click());
        // 检查View中是否包含"震动"
        onView(withText("震动")).check(matches(isDisplayed()));

        // 点击闹钟方式按钮
        onView(withId(R.id.holdSleep_icon)).perform(click());
        // 检测Dialog是否显示"震动并响铃"
        onView(withText("震动并响铃")).check(matches(isDisplayed()));
        // 点击Dialog"震动并响铃"
        onView(withText("震动并响铃")).perform(click());
        // 检查View中是否包含"震动"
        onView(withText("震动并响铃")).check(matches(isDisplayed()));

    }

    @Test
    public void selectFabOnclick() throws Exception {
        // TODO: 2016/12/21 测试未通过
        // 编辑Item
        onView(withText("test1")).perform(click());
        //是否为激活状态
        onView(withId(R.id.starred))
                .check(matches(MMatcher.isDrawableNotifyON(1)));
        //点击FloatButton按钮
        onView(withId(R.id.starred)).perform(click());
        //是否为非激活状态
        onView(withId(R.id.starred))
                .check(matches(MMatcher.isDrawableNotifyON(2)));

    }
}
