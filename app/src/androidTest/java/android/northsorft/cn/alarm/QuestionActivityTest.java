package android.northsorft.cn.alarm;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.ui.activity.questionActivity.QuestionActivity;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * Created by Xushudi on 2016/12/20 下午7:10.
 */
@RunWith(AndroidJUnit4.class)
public class QuestionActivityTest {

    @Rule
    public ActivityTestRule<QuestionActivity> mActivityRule = new ActivityTestRule<>(
            QuestionActivity.class);

    @Test
    public void getInfo() throws Exception {
        // TODO: 2016/12/23 测试未通过
        onView(withId(R.id.answer_ed)).perform(typeText("test02"));
        Assert.assertEquals(mActivityRule.getActivity().getInfo(),"test02");
    }

    @Test
    public void setInfo() throws Exception {
        // TODO: 2016/12/23 测试未通过
        AlarmModelVo alarmModelVo = new AlarmModelVo("1","test","11:22"
                ,"重复一次","1","响铃","true",new int[]{5,9,14});
        mActivityRule.getActivity().setInfo(alarmModelVo);
        //检测问题设置正确性
        onView(withId(R.id.question_text)).check(matches(withText("5+9=?")));
        //检测时间设置正确性
        String s[] = alarmModelVo.getTime().split(":");
        onView(withId(R.id.time_hour_text_q)).check(matches(withText(s[0])));
        onView(withId(R.id.time_minute_text_q)).check(matches(withText(s[1])));

    }

    @Test
    public void onDestroy() throws Exception {

    }

    @Test
    public void onClick() throws Exception {
        // TODO: 2016/12/23 测试未通过
        QuestionActivity activity = mActivityRule.getActivity();
        //检测答案错误时是否弹窗
        activity.testOnclick=false;
        onView(withId(R.id.fra_sure_btn)).perform(click());
        onView(withText("答案错误"))
                .inRoot(withDecorView(not(activity.getWindow()
                        .getDecorView()))).check(matches(isDisplayed()));
        //检测答案确实时是否Destroy
        activity.testOnclick=true;
        onView(withId(R.id.fra_sure_btn)).perform(click());
        Assert.assertEquals(activity.isDestroyed(),true);

 }



}
