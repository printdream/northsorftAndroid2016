package android.northsorft.cn.alarm.ui.activity.editAlarmActivity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;

/**
 * Created by Xushudi on 2016/12/24 上午12:27.
 */

class SelectRingOnclick {
    EditAlarmActivity that;

    public SelectRingOnclick(EditAlarmActivity editAlarmActivity) {
        that = editAlarmActivity;
    }

    public void selectRingOnclick(View view) {
        //  2016/12/19 弹出AlertDialog。修改界面响铃方式数据
        /**
         * 说明
         * 创建并显示AlertDialog
         * AlertDialog标题为"选择方式"
         * AlertDialog内容为"震动"、"响铃"、"震动并响铃"
         * 设置单选点击事件
         * 选择相应内容时如""震动"、"响铃"、"震动并响铃""
         * 设置"that.alarmModelVo.setRing"为相应重复类型如"震动"
         * 设置"that.mRingText.setText"为相应重复类型如"震动"
         *
         */
        //王建军
        final String[] options = new String[]{"震动", "响铃", "震动并响铃"};
        AlertDialog.Builder builder = new AlertDialog.Builder(that);
        builder.setTitle("选择方式");
        //单选
        builder.setSingleChoiceItems(options, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String ring = options[which];
                that.mRingText.setText(ring);
                that.alarmModelVo.setRing(ring);
                dialog.dismiss();
            }
        });
        //显示Dialog
        AlertDialog dialog = builder.create();
        dialog.show();


    }

}
