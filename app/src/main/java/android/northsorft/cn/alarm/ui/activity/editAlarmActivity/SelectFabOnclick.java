package android.northsorft.cn.alarm.ui.activity.editAlarmActivity;

import android.northsorft.cn.alarm.R;
import android.view.View;

/**
 * Created by Xushudi on 2016/12/24 上午12:30.
 */

class SelectFabOnclick {
    EditAlarmActivity that;
    SelectFabOnclick(EditAlarmActivity editAlarmActivity) {
        that = editAlarmActivity;
    }
    void selectFabOnclick(View view) {
        //  2016/12/19 点击闹钟激活图标时修改图标并设置是否激活
        /**
         * 说明
         * 当"Boolean.valueOf(that.alarmModelVo.getActive())"为真时
         * 设置"that.mFAB.setIcon"为"R.drawable.ic_notifications_off_grey600_24dp"
         * 设置"that.alarmModelVo.setActive"为""false""
         * 当"Boolean.valueOf(that.alarmModelVo.getActive())"为假时
         * 设置"that.mFAB.setIcon"为"R.drawable.ic_notifications_on_white_24dp"
         *  设置"that.alarmModelVo.setActive"为""true""
         *
         */
        //金钛鑫
        if (Boolean.valueOf(that.alarmModelVo.getActive())){
            that.mFAB.setIcon(R.drawable.ic_notifications_off_grey600_24dp);
            that.alarmModelVo.setActive("false");
        }else{
            that.mFAB.setIcon(R.drawable.ic_notifications_on_white_24dp);
            that.alarmModelVo.setActive("true");
        }
    }
}
