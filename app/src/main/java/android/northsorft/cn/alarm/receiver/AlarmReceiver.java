package android.northsorft.cn.alarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.ui.MainView;
import android.northsorft.cn.alarm.ui.activity.MainActivity;
import android.northsorft.cn.alarm.ui.activity.questionActivity.QuestionActivity;
import android.util.Log;

/**
 * Created by Xushudi on 2016/12/14 下午11:14.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("alarm","闹钟啦！");
        // 启动PlayAlarmActivity
        Intent i = new Intent(context,QuestionActivity.class);
        AlarmModelVo alarmModelVo = (AlarmModelVo) intent.getSerializableExtra(Flag.ID_FLAG);
        i.putExtra(Flag.ID_FLAG,alarmModelVo);
        //无视Stack
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //启动Activity
        context.startActivity(i);
    }
}
