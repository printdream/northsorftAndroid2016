package android.northsorft.cn.alarm.presenter;

import android.content.Context;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.service.impl.AlarmServiceImpl;
import android.northsorft.cn.alarm.ui.RecyclerView;
import android.util.Log;

import java.util.List;

/**
 * Created by Xushudi on 2016/12/16p 下午2:50.
 */

public class PresenterRecycler {
    private RecyclerView recyclerView;
    private AlarmService alarmService;
    private Context context;

    public PresenterRecycler(RecyclerView recyclerView, Context context) {
        this.recyclerView = recyclerView;
        this.alarmService = new AlarmServiceImpl(context);
        this.context = context;
    }

    /**
     * 设置RecyclerView初始化数据
     *
     * @throws Exception
     */
    public void setRecyclerViewData() throws Exception {
        List<AlarmModelVo> alarmModelVos;
        alarmModelVos = alarmService.queryAllAlarm();
        recyclerView.setRecyclerView(alarmModelVos);
    }

    /**
     * 删除数据库中闹钟相应数据
     * @throws Exception
     */
    public void deleteAlarm() throws Exception{
        //  2016/12/16 删除数据库中对应ID闹钟数据
        alarmService.deleteAnAlarm(recyclerView.getDeleteAlarm().getTitle());
    }

    /**
     * 使能数据库中闹钟相应数据
     * @throws Exception
     */
    public void enableAlarm(AlarmModelVo alarmModelVo) throws Exception{
        //  2016/12/16 设置数据库中是否激活铃声
        alarmService.alterAnAlarm(alarmModelVo);

    }
}
