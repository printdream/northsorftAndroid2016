package android.northsorft.cn.alarm.presenter;

import android.content.Context;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.service.impl.AlarmServiceImpl;
import android.northsorft.cn.alarm.ui.MainView;
import android.northsorft.cn.alarm.ui.RecyclerView;

/**
 * Created by Xushudi on 2016/12/14 上午10:29.
 */

public class PresenterMain {
    private MainView mainView;
    private RecyclerView recyclerView;
    private AlarmService alarmService;
    private Context context;

    public PresenterMain(MainView mainView,Context context) {
        this.mainView = mainView;
        this.context = context;
        alarmService = new AlarmServiceImpl(context);

    }


}
