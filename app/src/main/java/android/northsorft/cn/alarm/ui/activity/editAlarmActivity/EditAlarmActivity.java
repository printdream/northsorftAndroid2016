package android.northsorft.cn.alarm.ui.activity.editAlarmActivity;

import android.content.Intent;
import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.presenter.PresenterEditAlarm;
import android.northsorft.cn.alarm.ui.EditView;
import android.northsorft.cn.alarm.utils.Utils;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

/**
 * Created by Xushudi on 2016/12/18 下午10:49.
 */

public class EditAlarmActivity extends AppCompatActivity implements EditView ,TimePickerDialog.OnTimeSetListener {
    AlarmModelVo alarmModelVo;
    Toolbar mToolbar;
    EditText mEditText;
    TextView mTimeText, mRepeatText, mRingText;
    FloatingActionButton mFAB;
    PresenterEditAlarm presenterEditAlarm;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_alarm);
        presenterEditAlarm = new PresenterEditAlarm(this,getApplicationContext());

        init();
    }
    void init(){
        Intent i = getIntent();
        alarmModelVo = (AlarmModelVo) i.getSerializableExtra(Flag.ID_FLAG);
        // Find view by id
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mEditText = (EditText) findViewById(R.id.alarm_title);
        mTimeText = (TextView) findViewById(R.id.set_time);
        mRepeatText = (TextView) findViewById(R.id.set_repeat);
        mRingText = (TextView) findViewById(R.id.set_ring);

        mFAB = (FloatingActionButton) findViewById(R.id.starred);
        // 设置view初始化数据
        mEditText.setText(alarmModelVo.getTitle());
        mTimeText.setText(alarmModelVo.getTime());
        mRepeatText.setText(alarmModelVo.getRepeatType());
        mRingText.setText(alarmModelVo.getRing());
        if (!Boolean.valueOf(alarmModelVo.getActive())){
            mFAB.setIcon(R.drawable.ic_notifications_off_grey600_24dp);
        }
        //配置ToolBar
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("设置闹钟");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        tittleChangeListener();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_alarm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.save_Alarm:
                //保存数据到数据库
                presenterEditAlarm.setViewDataToDb();
                onBackPressed();
                return true;

            case R.id.discard_alarm:
                Toast.makeText(getApplicationContext(), "取消设置",
                        Toast.LENGTH_SHORT).show();

                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void tittleChangeListener(){
        //标题EditText文字改变监听 取消警告信息
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //去空格
                alarmModelVo.setTitle(s.toString().trim());
                mEditText.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
    //设置时间点击事件
    @Override
    public void selectTimeOnclick(View view) {
        int times[] = Utils.stringToInts(alarmModelVo.getTime());
        Calendar now = Calendar.getInstance();
        TimePickerDialog timeDialog = TimePickerDialog.newInstance(
                this, times[0], times[1], false);
        //不使用黑色主题
        timeDialog.setThemeDark(false);
        //显示
        timeDialog.show(getFragmentManager(), "选择时间");
    }
    //时间控件设置后回调事件
    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        Log.i("tag", "onTimeSet: "+hourOfDay+"-"+minute);
        String time = hourOfDay+":"+minute;
        alarmModelVo.setTime(time);
        mTimeText.setText(time);
    }

    @Override
    public void selectRepeatOnclick(View view) {
        new SelectRepeatOnclick(this).selectRepeatOnclick(view);
    }

    @Override
    public void selectRingOnclick(View view) {
        new SelectRingOnclick(this).selectRingOnclick(view);
    }

    @Override
    public void selectFabOnclick(View view) {
        new SelectFabOnclick(this).selectFabOnclick(view);
    }

    @Override
    public AlarmModelVo getInfo() {
        return alarmModelVo;
    }

    @Override
    public void setInfo(AlarmModelVo alarmModelVo) {

    }

}
