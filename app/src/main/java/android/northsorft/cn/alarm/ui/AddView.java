package android.northsorft.cn.alarm.ui;

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.view.View;

/**
 * Created by Xushudi on 2016/12/18 下午10:47.
 */

public interface AddView {
    /**
     * 时间点击事件
     */
    public void selectTimeOnclick(View view);

    /**
     * 重复类型点击事件
     */
    public void selectRepeatOnclick(View view);

    /**
     * 铃声选择点击时间
     */
    public void selectRingOnclick(View view);

    /**
     * 激活或取消激活按钮点击事件
     * @param view
     */
    public void selectFabOnclick(View view);
    /**
     * 获取界面信息
     * @return
     */
    public AlarmModelVo getInfo();


}
