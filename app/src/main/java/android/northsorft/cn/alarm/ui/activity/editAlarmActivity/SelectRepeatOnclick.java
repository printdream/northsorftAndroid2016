package android.northsorft.cn.alarm.ui.activity.editAlarmActivity;

import android.content.DialogInterface;
import android.northsorft.cn.alarm.R;
import android.support.v7.app.AlertDialog;
import android.view.View;

/**
 * Created by Xushudi on 2016/12/24 上午12:01.
 */

class SelectRepeatOnclick {
    EditAlarmActivity that;
    SelectRepeatOnclick(EditAlarmActivity editAlarmActivity) {
        that = editAlarmActivity;
    }

    void selectRepeatOnclick(View view) {
        //  2016/12/19 弹出AlertDialog。修改界面重复类型数据
        /**
         * 说明
         * 创建并显示AlertDialog
         * AlertDialog标题为"选择重复"
         * AlertDialog内容为"只响一次"
         * AlertDialog图标为"R.drawable.ic_view_day_grey600_24dp"
         * 设置单选点击事件
         * 选择"只响一次"时
         * 设置"that.alarmModelVo.setRepeatType"为相应重复类型如"只响一次"
         * 设置"that.mRepeatText.setText"为相应重复类型如"只响一次"
         */
        //宣照磊
        // HIGH
        new AlertDialog.Builder(that)
                .setTitle("选择重复")
                .setIcon(R.drawable.ic_view_day_grey600_24dp)
                .setSingleChoiceItems(new String[]{"只响一次"}, 0,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                that.alarmModelVo.setRepeatType("只响一次");
                                that.mRepeatText.setText("只响一次");
                            }
                        }).show();

    }
}
