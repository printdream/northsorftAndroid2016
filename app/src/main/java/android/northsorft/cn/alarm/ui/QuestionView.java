package android.northsorft.cn.alarm.ui;

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

/**
 * Created by Xushudi on 2016/12/19 上午2:08.
 */

public interface QuestionView {
    /**
     * 获取信息
     * @return
     */
    public String getInfo();

    /**
     * 设置信息
     * @param alarmModelVo
     */
    public void setInfo(AlarmModelVo alarmModelVo);
    public String getID();
}
