package android.northsorft.cn.alarm.ui.activity.questionActivity;

import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.presenter.PresenterQuestion;
import android.northsorft.cn.alarm.ui.QuestionView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Xushudi on 2016/12/19 上午2:09.
 */

public class QuestionActivity extends AppCompatActivity implements QuestionView,View.OnClickListener {
    TextView question_te,questionTittle,hourText,minuteText;
    EditText answer_ed;
    Button sure_btn;
    String id;
//    PresenterQuestion presenterQuestion;

    // TODO: 2016/12/23 测试用待删除
    public boolean testOnclick;
    public PresenterQuestion presenterQuestion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_alarm);
        init();
    }

    private void init() {
        AlarmModelVo alarmModelVo = (AlarmModelVo) getIntent().getSerializableExtra(Flag.ID_FLAG);
        id = alarmModelVo.getId();

        presenterQuestion = new PresenterQuestion(this,this.getApplicationContext());

        // TODO: 2016/12/20 findViewByID 、设置提交按钮点击事件传递给this、播放铃声或震动
        hourText = (TextView) findViewById(R.id.time_hour_text_q);
        minuteText = (TextView) findViewById(R.id.time_minute_text_q);
        answer_ed = (EditText) findViewById(R.id.answer_ed);
        sure_btn = (Button) findViewById(R.id.fra_sure_btn);
        // 时间监听
        sure_btn.setOnClickListener(this);

    }

    @Override
    public String getInfo() {
        return new GetInfo(this).getInfo();
    }

    @Override
    public void setInfo(AlarmModelVo alarmModelVo) {
        new SetInfo(this).setInfo(alarmModelVo);
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // TODO: 2016/12/20 停止播放闹钟
    }

    @Override
    public void onClick(View view) {
        new OnClick(this).onClick(view);
    }
}
