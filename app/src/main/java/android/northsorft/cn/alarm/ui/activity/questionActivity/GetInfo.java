package android.northsorft.cn.alarm.ui.activity.questionActivity;

/**
 * Created by Xushudi on 2016/12/24 上午12:43.
 */

class GetInfo {
    QuestionActivity that;
    public GetInfo(QuestionActivity that) {
        this.that = that;
    }

    public String getInfo() {
        // TODO: 2016/12/20 获取View中问题答案
        /**
         * 说明
         * 在"that.answer_ed"类中获取EditText控件内文字答案并return
         */
        return that.answer_ed.getText().toString();
    }
}
