package android.northsorft.cn.alarm.presenter;

import android.content.Context;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.service.impl.AlarmServiceImpl;
import android.northsorft.cn.alarm.ui.EditView;

/**
 * Created by Xushudi on 2016/12/19 下午6:00.
 */

public class PresenterEditAlarm {
    private EditView editView;
    private AlarmService alarmService;
    private Context context;
    public PresenterEditAlarm(EditView editView, Context context) {
        this.editView = editView;
        this.context = context;
        alarmService = new AlarmServiceImpl(context);
    }
    public void setViewDataToDb(){
        try {
            alarmService.alterAnAlarm(editView.getInfo());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
