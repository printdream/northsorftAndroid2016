package android.northsorft.cn.alarm.ui.activity.addAlarmActivity;

import android.content.DialogInterface;
import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.ui.activity.editAlarmActivity.EditAlarmActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;

/**
 * Created by Xushudi on 2016/12/24 上午12:01.
 */

class SelectRepeatOnclick {
    AddAlarmActivity that;
    public SelectRepeatOnclick(AddAlarmActivity addAlarmActivity) {
        that = addAlarmActivity;
    }

    public void selectRepeatOnclick(View view) {
        // 2016/12/19 弹出AlertDialog。修改界面重复类型数据
        /**
         * 说明
         * 创建并显示AlertDialog
         * AlertDialog标题为"选择重复"
         * AlertDialog内容为"只响一次"
         * AlertDialog图标为"R.drawable.ic_view_day_grey600_24dp"
         * 设置单选点击事件
         * 选择"只响一次"时
         * 设置"that.alarmModelVo.setRepeatType"为相应重复类型如"只响一次"
         * 设置"that.mRepeatText.setText"为相应重复类型如"只响一次"
         */
        // 单宝伟
        final String[] items = {"只响一次"};
        // AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(that);
        builder.setIcon(R.drawable.ic_view_day_grey600_24dp);
        builder.setTitle("选择重复");
        // 设置Items
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String repeatType = items[which];
                that.mRepeatText.setText(repeatType);
                that.alarmModelVo.setRepeatType(repeatType);
            }
        });
        //显示
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
