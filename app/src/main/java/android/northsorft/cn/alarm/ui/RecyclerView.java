package android.northsorft.cn.alarm.ui;

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

import java.util.List;

/**
 * Created by Xushudi on 2016/12/15 下午5:37.
 */

public interface RecyclerView {

    /**
     * 设置RecyclerView相关数据
     * @param alarmModelVoList
     * @throws Exception
     */
    public void setRecyclerView(List<AlarmModelVo> alarmModelVoList) throws Exception;

    /**
     * 获取要删除的闹钟
     * @throws Exception
     */
    public AlarmModelVo getDeleteAlarm() throws Exception;

    /**
     * 获取使能闹钟
     * @return
     * @throws Exception
     */
    public AlarmModelVo getEnableAlarm() throws Exception;
}
