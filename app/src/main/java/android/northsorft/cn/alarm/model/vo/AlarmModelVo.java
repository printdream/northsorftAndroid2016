package android.northsorft.cn.alarm.model.vo;

import java.io.Serializable;

/**
 * Created by Xushudi on 2016/12/13 下午4:14.
 */

public class AlarmModelVo implements Serializable {
    private String id;
    private String title;
    private String time;
    private String repeatType;
    private String repeatCode;
    private String ring;
    private String active;
    private int[] radomQuestion;

    public int[] getRadomQuestion() {
        return radomQuestion;
    }

    public void setRadomQuestion(int[] radomQuestion) {
        this.radomQuestion = radomQuestion;
    }

    @Override
    public String toString() {
        return "AlarmModelVo{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", time='" + time + '\'' +
                ", repeatType='" + repeatType + '\'' +
                ", repeatCode='" + repeatCode + '\'' +
                ", ring='" + ring + '\'' +
                ", active='" + active + '\'' +
                '}';
    }

    public AlarmModelVo() {
    }
    public AlarmModelVo(String id, String title, String time
            , String repeatType, String repeatCode, String ring, String active, int[] radomQuestion) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.repeatType = repeatType;
        this.repeatCode = repeatCode;
        this.ring = ring;
        this.active = active;
        this.radomQuestion = radomQuestion;
    }
    public AlarmModelVo(String id, String title, String time
            , String repeatType, String repeatCode, String ring, String active) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.repeatType = repeatType;
        this.repeatCode = repeatCode;
        this.ring = ring;
        this.active = active;
    }

    public String getRing() {
        return ring;
    }

    public void setRing(String ring) {
        this.ring = ring;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(String repeatType) {
        this.repeatType = repeatType;
    }

    public String getRepeatCode() {
        return repeatCode;
    }

    public void setRepeatCode(String repeatCode) {
        this.repeatCode = repeatCode;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
