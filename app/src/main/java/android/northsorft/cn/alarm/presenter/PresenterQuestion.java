package android.northsorft.cn.alarm.presenter;

import android.content.Context;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.service.impl.AlarmServiceImpl;
import android.northsorft.cn.alarm.ui.QuestionView;

/**
 * Created by Xushudi on 2016/12/20 上午9:08.
 */

public class PresenterQuestion {
    private QuestionView questionView;
    private AlarmService alarmService;
    private Context context;
    private AlarmModelVo alarmModelVo;

    public PresenterQuestion(QuestionView questionView, Context context) {
        this.alarmService = new AlarmServiceImpl(context);
        this.questionView = questionView;
        this.context = context;
    }
    // 设置数据到View
    public void setDataToView() throws Exception {
        alarmModelVo = alarmService.queryAnAlarm(questionView.getID());
        questionView.setInfo(alarmModelVo);
    }

    // 验证答案是否与题目答案相符
    public boolean questionEqualAnswer(){

        return  questionView.getInfo()
                .equals(alarmModelVo.getRadomQuestion()[2]+"");
    }



    //  2016/12/23 测试用待删除
    // (测试用)验证答案是否与题目答案相符
//    public boolean questionEqualAnswer(boolean b){
//        return  b;
//    }

}
