package android.northsorft.cn.alarm.utils;

import java.util.Calendar;

/**
 * Created by Xushudi on 2016/12/15 上午12:02.
 */

public class Utils {
    /**
     * 生成随机问题和答案
     * @return
     */
    public static int[] radomQuestion(){
        //  2016/12/23 随机数生成
        //刘春岩
        /**
         * 说明：
         * 返回整形数组  2个百位随机数 及2个数的求和答案
         * 如 生成随机数 234 和 345 及返回的数组应为
         * 234+345=579
         * i[0]=234 i[1]=345 i[3]=579
         * 使用 Math.random()生成随机数（其数值大小为浮点型小数如0.2443）
         * 变换生成的随机数值使其为百位数
         * 计算两数和答案
         * 构建整形数组并返回数组
         */
        int ret[] = new int[3];
        ret[0] = (int) (Math.random() * 1000);
        ret[1] = (int) (Math.random() * 1000);
        ret[2] = ret[0] + ret[1];
        return ret;
    }
    /**
     * 字符串时间转换TimeMillis
     * @param time-例如"21:22"
     * @return
     */
    public static long stringToTimeMillis(String time){
        final int DAY_TIME = 1000 * 60 * 60 * 24;
        //分割时间
        String[] mTimeSplit = time.split(":");
        int mHour = Integer.parseInt(mTimeSplit[0]);
        int mMinute = Integer.parseInt(mTimeSplit[1]);
        //构建Calendar
        Calendar c = Calendar.getInstance();
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH), mHour, mMinute, 0);
        long mTimeInfo = c.getTimeInMillis();
        long actualTime = mTimeInfo > System.currentTimeMillis()
                ? mTimeInfo : mTimeInfo + (DAY_TIME);
        return actualTime;
    }

    /**
     * 字符串转换为 H、Min
     * @param s
     * @return
     */
    public static int[] stringToInts(String s){
        int ret[] = new int[2];
        String times[] = s.split(":");
        ret[0] = Integer.valueOf(times[0]);
        ret[1] = Integer.valueOf(times[1]);
        return ret;
    }

}
