package android.northsorft.cn.alarm.constants;

import android.database.sqlite.SQLiteDatabase;
import android.northsorft.cn.alarm.dao.AlarmSqlHelper;

/**
 * 常量
 * Created by Xushudi on 2016/12/14 下午11:59.
 */

public class Flag {
    public static final String ID_FLAG="id";


    // DB常量
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = " AlarmDatabase";
    public static final String TABLE_ALARM = " AlarmTable";
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_TIME = "time";
    public static final String KEY_REPEAT_TYPE = "repeat_type";
    public static final String KEY_REPEAT_CODE = "repeat_code";
    public static final String KEY_ACTIVE = "active";
    public static final String KEY_RING = "ring";


}
