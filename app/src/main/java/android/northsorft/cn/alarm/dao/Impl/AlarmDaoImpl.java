package android.northsorft.cn.alarm.dao.Impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.dao.AlarmDao;
import android.northsorft.cn.alarm.dao.AlarmSqlHelper;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

import java.util.ArrayList;
import java.util.List;

/**
 * 闹钟数据库dao层实现
 * Created by Xushudi on 2016/12/13 下午4:00.
 */

public class AlarmDaoImpl implements AlarmDao {
    private SQLiteDatabase db;
    private AlarmSqlHelper alarmSqlHelper;

    public AlarmDaoImpl(Context context) {
        AlarmSqlHelper openHelper = new AlarmSqlHelper(context, Flag.DATABASE_NAME,null,Flag.DATABASE_VERSION);
        this.alarmSqlHelper = openHelper;
        db=openHelper.getWritableDatabase();
    }

    // 2016/12/14 闹钟添加
    @Override
    public String addAlarm(AlarmModelVo vo) throws Exception {
        ContentValues values = new ContentValues();

        values.put(Flag.KEY_TITLE , vo.getTitle());
        values.put(Flag.KEY_TIME, vo.getTime());
        values.put(Flag.KEY_REPEAT_TYPE, vo.getRepeatType());
        values.put(Flag.KEY_REPEAT_CODE,vo.getRepeatCode());
        values.put(Flag.KEY_RING,vo.getRing());
        values.put(Flag.KEY_ACTIVE, vo.getActive());

        if (!db.isOpen()){
            db = alarmSqlHelper.getWritableDatabase();
        }

        long ID = db.insert(Flag.TABLE_ALARM,null,values);
        db.close();
        return ID+"";
    }

    //  2016/12/14 闹钟删除
    @Override
    public void deleteAnAlarm(String id) throws Exception {
        if (!db.isOpen()){
            db = alarmSqlHelper.getWritableDatabase();
        }
        db.delete(Flag.TABLE_ALARM, Flag.KEY_ID + "=?",
                new String[]{id});
        db.close();

    }

    //  2016/12/14 修改闹钟
    @Override
    public void alterAnAlarm(AlarmModelVo vo) throws Exception {
        ContentValues values = new ContentValues();

        values.put(Flag.KEY_TITLE , vo.getTitle());
        values.put(Flag.KEY_TIME , vo.getTime());
        values.put(Flag.KEY_REPEAT_TYPE, vo.getRepeatType());
        values.put(Flag.KEY_REPEAT_CODE, vo.getRepeatCode());
        values.put(Flag.KEY_RING,vo.getRing());
        values.put(Flag.KEY_ACTIVE, vo.getActive());

        if (!db.isOpen()){
            db = alarmSqlHelper.getWritableDatabase();
        }

        db.update(Flag.TABLE_ALARM, values, Flag.KEY_ID + "=?",
                new String[]{vo.getId()});
    }

    // 2016/12/14 通过ID查询单个闹钟信息
    @Override
    public AlarmModelVo queryAnAlarm(String id) throws Exception {
        if (!db.isOpen()){
            db = alarmSqlHelper.getWritableDatabase();
        }
        Cursor cursor = db.query(Flag.TABLE_ALARM,new String[]
                {
                        Flag.KEY_ID,
                        Flag.KEY_TITLE,
                        Flag.KEY_TIME,
                        Flag.KEY_REPEAT_TYPE,
                        Flag.KEY_REPEAT_CODE,
                        Flag.KEY_RING,
                        Flag.KEY_ACTIVE
                },Flag.KEY_ID + "=?",new String[]{id},null,null,null,null);


        AlarmModelVo alarmModelVo = null;
        if (cursor != null && cursor.moveToFirst()) {

            alarmModelVo = new AlarmModelVo(cursor.getString(0)
                    ,cursor.getString(1),cursor.getString(2),
                    cursor.getString(3),cursor.getString(4),cursor.getString(5),
                    cursor.getString(6));
            cursor.close();
        }

        return alarmModelVo;
    }

    //  2016/12/14 查询全部闹钟信息
    @Override
    public List<AlarmModelVo> queryAllAlarm() throws Exception {
        List<AlarmModelVo> list = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + Flag.TABLE_ALARM;

        if (!db.isOpen()){
            db = alarmSqlHelper.getWritableDatabase();
        }

        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                AlarmModelVo alarmModel = new AlarmModelVo();
                alarmModel.setId(cursor.getString(0));
                alarmModel.setTitle(cursor.getString(1));
                alarmModel.setTime(cursor.getString(2));
                alarmModel.setRepeatType(cursor.getString(3));
                alarmModel.setRepeatCode(cursor.getString(4));
                alarmModel.setRing(cursor.getString(5));
                alarmModel.setActive(cursor.getString(6));

                list.add(alarmModel);
            } while (cursor.moveToNext());

        }
        cursor.close();
        return list;
    }
}
