package android.northsorft.cn.alarm.service.impl;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.dao.AlarmDao;
import android.northsorft.cn.alarm.dao.Impl.AlarmDaoImpl;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.ui.activity.questionActivity.QuestionActivity;
import android.northsorft.cn.alarm.utils.Utils;
import android.os.Vibrator;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * 闹钟服务
 * 1).修改数据库 2.)修改AlarmManger
 * Created by Xushudi on 2016/12/13 下午4:28.
 */

public class AlarmServiceImpl implements AlarmService {
    Context context;
    AlarmDao alarmDao;
    Vibrator vibrator;
    MediaPlayer player;

    public AlarmServiceImpl(Context context) {
        this.context = context;
        alarmDao = new AlarmDaoImpl(context);
    }

    @Override
    public int[] getRadomQuestionAndKey() {
        return Utils.radomQuestion();
    }
    //  2016/12/14 添加闹钟服务
    @Override
    public void addAnAlarm(AlarmModelVo vo) throws Exception {
        alarmDao.addAlarm(vo);
    }

    //  2016/12/14 删除闹钟服务
    @Override
    public void deleteAnAlarm(String id) throws Exception {
        alarmDao.deleteAnAlarm(id);
    }

    // 2016/12/14 修改闹钟服务
    @Override
    public void alterAnAlarm(AlarmModelVo vo) throws Exception {
        alarmDao.alterAnAlarm(vo);
    }

    //  2016/12/14 查询单个闹钟服务
    @Override
    public AlarmModelVo queryAnAlarm(String id) throws Exception {
        return alarmDao.queryAnAlarm(id);
    }

    //  2016/12/14 查询全部闹钟服务
    @Override
    public List<AlarmModelVo> queryAllAlarm() throws Exception {
        //  2016/12/23  模拟数据修改
        //模拟数据 start
//        List<AlarmModelVo> alarmModelVos = new ArrayList<>();
//        AlarmModelVo alarmModelVo = new AlarmModelVo();
//        alarmModelVo.setTitle("test1");
//        alarmModelVo.setActive("true");
//        alarmModelVo.setRepeatCode("1");
//        alarmModelVo.setRepeatType("重复一次");
//        alarmModelVo.setTime("21:22");
//        alarmModelVo.setRing("响铃");
//        alarmModelVos.add(alarmModelVo);
//
//        AlarmModelVo alarmModelVo2 = new AlarmModelVo();
//        alarmModelVo2.setTitle("test9");
//        alarmModelVo2.setActive("false");
//        alarmModelVo2.setRepeatCode("1");
//        alarmModelVo2.setRepeatType("重复一次");
//        alarmModelVo2.setTime("1:22");
//        alarmModelVo.setRing("响铃");
//        alarmModelVos.add(alarmModelVo2);
        //模拟数据 end

        return alarmDao.queryAllAlarm();
//        return alarmModelVos;
    }

    // 2016/12/14 启动闹钟服务
    @Override
    public void startAlarm(AlarmModelVo vo) throws Exception {
        setAndroidServiceAlarm(vo);
    }

    // 2016/12/14 停止闹钟服务
    @Override
    public void stopAlarm(AlarmModelVo vo) throws Exception {

        //获取AlarmManager
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        String repeat = vo.getRepeatType();
        //根据类型判断取消方式
        if (repeat.equals("每天") || repeat.equals("只响一次")) {
            PendingIntent pi = PendingIntent.getBroadcast(context, Integer.parseInt(vo.getId()),
                    new Intent(context, QuestionActivity.class), 0);
            alarmManager.cancel(pi);
        }
    }

    //  2016/12/15 设置Android系统服务闹钟Alarm Manger
    @Override
    public void setAndroidServiceAlarm(AlarmModelVo alarmModelVo) throws Exception {
        long actualTime = Utils.stringToTimeMillis(alarmModelVo.getTime());
        //设置闹钟
        AlarmManager alarmManager
                = (AlarmManager) context
                .getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(context, QuestionActivity.class);
        intent.putExtra(Flag.ID_FLAG, (alarmModelVo.getId()));
        PendingIntent pI = PendingIntent.getBroadcast(context,
                Integer.parseInt(alarmModelVo.getId()), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, actualTime, pI);
    }


    @Override
    public void startBizi(int type) {
        switch (type){
            //响铃
            case 1:
                startRing();
                break;
            //震动
            case 2:
                startVibrate();
                break;
            //响铃+震动
            case 3:
                startRing();
                startVibrate();
                break;
        }
    }

    @Override
    public void stopBizi(int type) {
        switch (type){
            //响铃
            case 1:
                stopRing();
                break;
            //震动
            case 2:
                stopVibrate();
                break;
            //响铃+震动
            case 3:
                stopRing();
                stopVibrate();
                break;
        }
    }

    //开始震动
    private void startVibrate() {
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1000, 5000, 1000, 5000};
        vibrator.vibrate(pattern, 0);
    }

    //停止震动
    private void stopVibrate() {
        vibrator.cancel();
    }

    private void startRing() {
        if (player != null && player.isPlaying()) {
            player.stop();
            player.release();
            player = MediaPlayer.create(context, R.raw.ring01);
        } else {
            player = MediaPlayer.create(context, R.raw.ring01);

        }

        player.start();
        player.setLooping(true);
        //播放结束监听
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                //重新播放
                player.start();
                player.setLooping(true);
            }
        });
    }

    private void stopRing() {
        player.stop();
        player.release();
    }

}
