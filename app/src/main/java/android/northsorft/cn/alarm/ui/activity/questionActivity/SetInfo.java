package android.northsorft.cn.alarm.ui.activity.questionActivity;

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

/**
 * Created by Xushudi on 2016/12/24 上午12:47.
 */

public class SetInfo {
    QuestionActivity that;
    public SetInfo(QuestionActivity that) {
        this.that = that;
    }
    public void setInfo(AlarmModelVo alarmModelVo) {
        // TODO: 2016/12/20 设置数据到View
        /**
         * 说明
         * 设置数据到View中
         *
         * 1)设置hourText显示小时(例)
         *
         * //用":"分割数据中时间的小时和分钟 如"11:22"分割后
         * //time[0]内数据为"11" time[1]内数据为"22"
         * String time[] = alarmModelVo.getTime().split(":");
         * //设置view(hourText)的文字为time[0]及小时
         * that.hourText.setText(time[0]);
         *
         * 2)设置minuteText显示分钟
         *
         *
         * 3)设置question_text（设置问题）例如"1+1=?"
         *  // 数据为 alarmModelVo.getRadomQuestion()  返回整形数组类型
         *  // 数据格式: 例如数据中为 123+456=？
         *  // 其格式为 i[0] -问题左侧部分即123  i[1] -问题右侧部分即456
         *  // 设置view that.question_te.setText()
         *  // 例如 that.question_te.setText("123+321=?");
         */
        //孙福祥
        String times[] = alarmModelVo.getTime().split(":");
        that.hourText.setText(times[0]);
        that.minuteText.setText(times[1]);
        int q[] = alarmModelVo.getRadomQuestion();
        that.question_te.setText(q[0]+"+"+q[1]+"=?");

    }
}
