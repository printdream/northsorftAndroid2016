package android.northsorft.cn.alarm.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.northsorft.cn.alarm.constants.Flag;

/**
 * Created by Xushudi on 2016/12/19 下午12:46.
 */

public class AlarmSqlHelper extends SQLiteOpenHelper {

    String CREATE_ALARM_TABLE = "CREATE TABLE" + Flag.TABLE_ALARM +

            "("
            + Flag.KEY_ID + " INTEGER PRIMARY KEY,"
            + Flag.KEY_TITLE + " TEXT,"
            + Flag.KEY_TIME + " INTEGER,"
            + Flag.KEY_REPEAT_TYPE + " TEXT,"
            + Flag.KEY_REPEAT_CODE + " TEXT,"
            + Flag.KEY_RING + " TEXT,"
            + Flag.KEY_ACTIVE + " BOOLEAN" + ")";

    public AlarmSqlHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ALARM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion >= newVersion){
            return;
        }
        db.execSQL("DROP TABLE IF EXISTS " + Flag.TABLE_ALARM);
        onCreate(db);
    }
}
