package android.northsorft.cn.alarm.ui.activity.questionActivity;

import android.view.View;
import android.widget.Toast;

/**
 * Created by Xushudi on 2016/12/24 上午12:49.
 */

class OnClick {
    QuestionActivity that;

    public OnClick(QuestionActivity that) {
        this.that = that;
    }

    public void onClick(View view) {
        // 2016/12/20 调用比较问题与答案是否一致,不一致
        /**
         * 说明
         * 根据isEqual变量真假值判断执行相应方法
         * 真：
         *   一致时调用finish()方法
         * 假：
         *   不一致时Toast弹出"答案错误"
         *   Toast.makeText(that,"答案错误",Toast.LENGTH_SHORT).show();
         *
         */
        //吴洋
        //获取答案与问题是否一致 返回true-一致  返回false-不一致
        boolean isEqual = that.presenterQuestion
                .questionEqualAnswer();
        if (isEqual) {
            that.finish();
        } else {
            Toast.makeText(that,"答案错误",Toast.LENGTH_SHORT).show();
        }

    }

}
