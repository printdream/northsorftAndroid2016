package android.northsorft.cn.alarm.ui.activity.addAlarmActivity;

import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.service.AlarmService;
import android.northsorft.cn.alarm.service.impl.AlarmServiceImpl;
import android.northsorft.cn.alarm.ui.AddView;
import android.northsorft.cn.alarm.utils.Utils;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Xushudi on 2016/12/15 下午5:18.
 */

public class AddAlarmActivity extends AppCompatActivity implements AddView,TimePickerDialog.OnTimeSetListener{
     AlarmModelVo alarmModelVo;
     Toolbar mToolbar;
     EditText mEditText;
     TextView mTimeText, mRepeatText, mWakeText, mRingText;
     FloatingActionButton mFAB;
    AlarmService alarmService;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.add_alarm);
        alarmService = new AlarmServiceImpl(this);

        init();
    }
    void init(){
        // Find view by id
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mEditText = (EditText) findViewById(R.id.alarm_title);
        mTimeText = (TextView) findViewById(R.id.set_time);
        mRepeatText = (TextView) findViewById(R.id.set_repeat);
        mRingText = (TextView) findViewById(R.id.set_ring);

        mFAB = (FloatingActionButton) findViewById(R.id.starred);
        // toolBarConfig
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Alarm");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }
    @Override
    public void selectTimeOnclick(View view) {
        Calendar now = Calendar.getInstance();
        Date date = now.getTime();
        TimePickerDialog timeDialog = TimePickerDialog.newInstance(
                this, date.getHours(), date.getMinutes(), false);
        //不使用黑色主题
        timeDialog.setThemeDark(false);
        //显示
        timeDialog.show(getFragmentManager(), "选择时间");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        new OnTimeSet(this).onTimeSet(view,hourOfDay,minute);
    }

    @Override
    public void selectRepeatOnclick(View view) {
        new SelectRepeatOnclick(this).selectRepeatOnclick(view);
    }

    @Override
    public void selectRingOnclick(View view) {
        new SelectRingOnclick(this).selectRingOnclick(view);
    }

    @Override
    public void selectFabOnclick(View view) {
        new SelectFabOnclick(this).selectFabOnclick(view);

    }

    @Override
    public AlarmModelVo getInfo() {
        return alarmModelVo;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_alarm, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.save_Alarm:
                //设置标题
                alarmModelVo.setTitle(mEditText.toString());
                try {
                    alarmService.addAnAlarm(alarmModelVo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //  2016/12/23 保存到DB操作
                return true;

            case R.id.discard_alarm:
                Toast.makeText(getApplicationContext(), "取消设置",
                        Toast.LENGTH_SHORT).show();

                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
