package android.northsorft.cn.alarm.ui.activity;

import android.content.Intent;
import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.presenter.PresenterMain;
import android.northsorft.cn.alarm.ui.MainView;
import android.northsorft.cn.alarm.ui.activity.addAlarmActivity.AddAlarmActivity;
import android.northsorft.cn.alarm.ui.recyclerAdapter.RecyclerAdapter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.getbase.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements MainView, View.OnClickListener {
    private RecyclerView recyclerView;
    private PresenterMain presenterMain;
    private RecyclerAdapter adapter;
    private FloatingActionButton floatButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化
     */
    void init() throws Exception {
        // findViewById
        recyclerView = (RecyclerView) findViewById(R.id.main_recyclerView);
        floatButton = (FloatingActionButton) findViewById(R.id.add_reminder);

        // 设置添加闹钟按钮点击事件
        floatButton.setOnClickListener(this);

        // 配置 recyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));
        //创建并绑定Adapter
        this.adapter = new RecyclerAdapter(getApplicationContext());
        recyclerView.setAdapter(this.adapter);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //添加闹钟按钮
            case R.id.add_reminder: {
                // 2016/12/15 跳转到闹钟添加界面
                Intent intent = new Intent(this, AddAlarmActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            adapter.presenterRecycler.setRecyclerViewData();
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
