package android.northsorft.cn.alarm.ui.activity.addAlarmActivity;

import android.util.Log;

import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

/**
 * Created by Xushudi on 2016/12/24 下午11:10.
 */

public class OnTimeSet {
    AddAlarmActivity that;

    public OnTimeSet(AddAlarmActivity addAlarmActivity) {
        that = addAlarmActivity;
    }

    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        //  2016/12/19 修改界面时间数据
        /**
         * 说明
         * 将 hourOfDay(小时)、minute(分钟) 拼接为"小时:分钟"字符串例如"11:22"
         * 例 String time = hourOfDay+":"+minute;
         * 设置字符串到that.alarmModelVo.setTime()
         * 设置字符串到that.mTimeText.setText()
         */
        String time = hourOfDay + ":" + minute;
        that.alarmModelVo.setTime(time);
        that.mTimeText.setText(time);

    }

}
