package android.northsorft.cn.alarm.ui.recyclerAdapter;

import android.content.Context;
import android.content.Intent;
import android.northsorft.cn.alarm.R;
import android.northsorft.cn.alarm.constants.Flag;
import android.northsorft.cn.alarm.model.vo.AlarmModelVo;
import android.northsorft.cn.alarm.presenter.PresenterRecycler;
import android.northsorft.cn.alarm.ui.activity.editAlarmActivity.EditAlarmActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * RecyclerView Adapter
 * Created by Xushudi on 2016/12/15 下午5:31.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> implements android.northsorft.cn.alarm.ui.RecyclerView {
    public RecyclerAdapter(Context context) throws Exception {
        this.context = context;
        this.presenterRecycler = new PresenterRecycler(this, context);
        presenterRecycler.setRecyclerViewData();
    }

    /**
     * RecyclerView Adapter
     */
    private Context context;
    private List<AlarmModelVo> alarmModelVoList;
    public PresenterRecycler presenterRecycler;
    private AlarmModelVo clickAlarmModelVo;

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_main, parent, false);
        return new RecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {
        AlarmModelVo item = alarmModelVoList.get(position);
        holder.setId(item.getId());
        holder.setAlarmTitle(item.getTitle());
        holder.setAlarmTime(item.getTime());
        holder.setRepeatType(item.getRepeatType());
        holder.setActiveImage(item.getActive());
        holder.setAlarmVo(item);
    }

    @Override
    public int getItemCount() {
        return alarmModelVoList.size();
    }

    // Holder
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitleText, mTimeText, mRepeatText;
        private ImageView mActiveImage, mThumbnailImage, mOverFlowImage;
        private String id;
        private AlarmModelVo alarmModelVo;

        public ViewHolder(View itemView) {
            super(itemView);

            // item 内部view
            mTitleText = (TextView) itemView.findViewById(R.id.re_tittle);
            mTimeText = (TextView) itemView.findViewById(R.id.re_time);
            mRepeatText = (TextView) itemView.findViewById(R.id.re_repeatType);
            mActiveImage = (ImageView) itemView.findViewById(R.id.active_image);
            mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            mOverFlowImage = (ImageView) itemView.findViewById(R.id.delete_image);
            //设置点击事件监听
            itemView.setOnClickListener(this);
            itemView.setLongClickable(true);
            mActiveImage.setOnClickListener(this);
            mOverFlowImage.setOnClickListener(this);
        }

        // Item点击事件
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.active_image: {
                    try {
                        enableAlarm(alarmModelVo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case R.id.delete_image: {
                    try {
                        deleatAlarm(alarmModelVo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                // click itemView
                default: {
                    Intent i = new Intent(context, EditAlarmActivity.class);
                    i.putExtra(Flag.ID_FLAG,alarmModelVo);
                    i.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                    break;
                }
            }
        }

        public void setAlarmTitle(String title) {
            mTitleText.setText(title);
        }

        public void setAlarmTime(String time) {
            mTimeText.setText(time);
        }

        public void setRepeatType(String type) {
            mRepeatText.setText(type);
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setAlarmVo(AlarmModelVo alarmVo) {
            this.alarmModelVo = alarmVo;
        }

        //设置激活状态图标
        public void setActiveImage(String active) {
            if (active.equals("true")) {
                mActiveImage.setImageResource(R.drawable.ic_alarm_on_grey_600_24dp);
            } else if (active.equals("false")) {
                mActiveImage.setImageResource(R.drawable.ic_alarm_off_grey_600_24dp);
            }
        }

    }

    //使能闹钟
    public void enableAlarm(AlarmModelVo clickAlarmModelVo) throws Exception {
        AlarmModelVo alarmModelVo = null;
        for (int i = 0; i < alarmModelVoList.size(); i++) {
            alarmModelVo = alarmModelVoList.get(i);
            if (clickAlarmModelVo == alarmModelVo) {
                if (Boolean.valueOf(alarmModelVo.getActive())) {
                    alarmModelVo.setActive("false");
                    alarmModelVoList.set(i, alarmModelVo);
                } else {
                    alarmModelVo.setActive("true");
                    alarmModelVoList.set(i, alarmModelVo);
                }
                presenterRecycler.enableAlarm(alarmModelVo);
                break;
            }
        }
        setClickAlarmModelVo(alarmModelVo);
        // 更新RecyclerView
        notifyDataSetChanged();

        presenterRecycler.enableAlarm(alarmModelVo);
    }

    //删除闹钟
    public void deleatAlarm(AlarmModelVo clickAlarmModelVo) throws Exception {
        AlarmModelVo alarmModelVo = null;
        for (int i = 0; i < alarmModelVoList.size(); i++) {
            alarmModelVo = alarmModelVoList.get(i);
            if (clickAlarmModelVo == alarmModelVo) {
                alarmModelVoList.remove(i);
                break;
            }
        }
        setClickAlarmModelVo(alarmModelVo);
        notifyDataSetChanged();
        // 更新RecyclerView
        presenterRecycler.deleteAlarm();
    }

    public void setClickAlarmModelVo(AlarmModelVo clickAlarmModelVo) {
        this.clickAlarmModelVo = clickAlarmModelVo;
    }

    @Override
    public AlarmModelVo getDeleteAlarm() throws Exception {
        return clickAlarmModelVo;
    }

    @Override
    public AlarmModelVo getEnableAlarm() throws Exception {
        return clickAlarmModelVo;
    }

    @Override
    public void setRecyclerView(List<AlarmModelVo> alarmModelVoList) throws Exception {
        this.alarmModelVoList = alarmModelVoList;
    }

}
