package android.northsorft.cn.alarm.service;

/**
 * Created by Xushudi on 2016/12/13 下午3:47.
 */

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

import java.util.List;

/**
 * 闹钟服务层
 */
public interface AlarmService {
    /**
     * 新增闹钟
     *
     * @param vo-闹钟PO模型
     * @throws Exception
     */
    public void addAnAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 删除闹钟
     *
     * @param id
     * @throws Exception
     */
    public void deleteAnAlarm(String id) throws Exception;

    /**
     * 修改闹钟
     *
     * @param vo
     * @throws Exception
     */
    public void alterAnAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 查询单个闹钟
     *
     * @param id
     * @return
     * @throws Exception
     */
    public AlarmModelVo queryAnAlarm(String id) throws Exception;

    /**
     * 查询全部Alarm
     *
     * @return
     * @throws Exception
     */
    public List<AlarmModelVo> queryAllAlarm() throws Exception;

    /**
     * 启动闹钟(激活)
     *
     * @param vo
     * @throws Exception
     */
    public void startAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 停止闹钟
     *
     * @param vo
     * @throws Exception
     */
    public void stopAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 设置Android系统服务闹钟Alarm Manger
     *
     * @param alarmModelVo
     * @throws Exception
     */
    public void setAndroidServiceAlarm(AlarmModelVo alarmModelVo) throws Exception;

    /**
     * 获取随机问题和答案
     * @return int（[0]-问题） + （[1]-问题） =（[2]-答案）
     */
    public int[] getRadomQuestionAndKey();

    /**
     * 启动响铃或震动
     * @param type 1-响铃 2-震动 3-响铃和震动
     */
    public void startBizi(int type);

    /**
     * 停止响铃或震动
     * @param type 1-响铃 2-震动 3-响铃和震动
     */
    public void stopBizi(int type);
}
