package android.northsorft.cn.alarm.dao;

/**
 * Created by Xushudi on 2016/12/13 下午3:48.
 */

import android.northsorft.cn.alarm.model.vo.AlarmModelVo;

import java.util.List;

/**
 * 闹钟数据库dao层接口
 */
public interface AlarmDao {
    /**
     * 增加闹钟
     *
     * @param vo-闹钟Model
     * @return 返回ID
     * @throws Exception
     */
    public String addAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 删除单个闹钟
     *
     * @param id-查询ID
     * @throws Exception
     */
    public void deleteAnAlarm(String id) throws Exception;

    /**
     * 修改单个闹钟
     *
     * @param vo-闹钟Model
     * @throws Exception
     */
    public void alterAnAlarm(AlarmModelVo vo) throws Exception;

    /**
     * 查询单个闹钟
     *
     * @param id-查询ID
     * @return 返回单个闹钟Model
     * @throws Exception
     */
    public AlarmModelVo queryAnAlarm(String id) throws Exception;

    /**
     * 获取数据库中全部闹钟
     *
     *
     * @return 返回全部闹钟List
     * @throws Exception
     */
    public List<AlarmModelVo> queryAllAlarm() throws Exception;

}
